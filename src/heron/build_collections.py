"""
Build collections from files.

Give it a directory name. Get back a root collection.
"""
from datetime import datetime
from pathlib import Path
from typing import Dict, List, Any

import tomli
from tomli import TOMLDecodeError

from . import files_cache, images
from .transform import transform
from .utils import get_file_paths, get_dir_paths


def build(path: Path, prefix=None, parent=None):
    """Build a collection from a given path."""

    try:
        with open(path / "_attributes.toml", "rb") as f:
            attributes = tomli.load(f)
    except TOMLDecodeError:
        print(f"could not toml-decode file {path / '_attributes.toml'}")
        return Collection({"_parent": parent}, [], {})
    except FileNotFoundError:
        attributes = {}

    attributes["_filename"] = attributes["_basename"] = path.name
    if prefix is None:
        attributes["_url"] = _url = "/"
    else:
        attributes["_url"] = _url = prefix + path.name + "/"
    attributes["_parent"] = parent

    files = []
    collections = {}
    collection = Collection(attributes, files, collections)

    for fp in get_file_paths(path):
        files.append(fp)

    for dp in get_dir_paths(path):
        subcoll = build(dp, prefix=_url, parent=collection)
        if subcoll.attributes.get('include_in_parent'):
            files.extend(subcoll.files)
        else:
            collections[dp.name] = subcoll

    return collection


def process_collection(collection, include_drafts):

    def sort_key(f):
        d = f["date"]
        if isinstance(d, datetime):
            return d.timestamp()
        else:
            return datetime(d.year, d.month, d.day).timestamp()

    collection.files = [
        process_file(fp, collection, include_drafts=include_drafts)
        for fp in collection.files
    ]
    collection.files = [f for f in collection.files if f is not None]
    collection.files.sort(key=sort_key, reverse=True)

    for subcollection in collection.collections.values():
        process_collection(subcollection, include_drafts=include_drafts)

    return collection


def process_file(fp, collection: "Collection", include_drafts):

    cached_file = files_cache.get(fp)
    if cached_file.c_build_content:
        for image, params in cached_file.c_requested_images:
            images.render(image, **params)
        return cached_file.c_build_content

    base_url = collection.attributes["_url"]
    file, contents, transformed, requested_images = read_file(fp)
    if file.get("status") == "draft" and not include_drafts:
        return None
    file["_filename"] = fp.name
    file["_basename"] = fp.stem
    file["_contents"] = transformed
    file["_raw_content"] = contents
    if file.get("date") is not None:
        pass  # file["date"] = datetime.strptime(file["date"], "")
    else:
        file["date"] = datetime.fromtimestamp(fp.stat().st_mtime)
    file["_url"] = f"{base_url}{fp.stem}.html"
    file["_parent"] = collection
    if file.get("status") == "draft":
        files_cache.remove(fp)
    else:
        cached_file.c_build_content = file
        cached_file.c_requested_images = requested_images

    return file


class Collection:
    """
    A collection of files (and possibly sub-collections).

    All front matter of a collection is in a dictionary `attributes`, files are in a list `files`, sub-collections in
    a dictionary `collections {name: collection}`.
    """

    def __init__(self, attributes, files, collections):
        self.attributes = attributes
        self.files: List[Dict] = files
        self.collections: Dict[str, Collection] = collections

    def __iter__(self):
        return iter(self.files)

    def __getitem__(self, item):
        if item == "name" and "name" not in self.attributes:
            return self.attributes["_basename"]
        return self.attributes[item]

    def count_files(self):
        return len(self.files) + sum(c.count_files() for c in self.collections.values())

    def count_collections(self):
        return 1 + sum(c.count_collections() for c in self.collections.values())


def read_file(path: Path) -> (dict[str, Any], str, list[tuple]):
    def get_separation_indices():
        try:
            first_index = contents.index("---\n")
        except ValueError:
            return None, None
        try:
            second_index = contents.index("---\n", first_index + 1)
        except ValueError:
            return first_index, None
        return first_index, second_index

    contents = path.read_text()
    first_index, second_index = get_separation_indices()
    if second_index is None:
        if first_index is None:
            head, contents = "", contents
            return {}, contents, []
        else:
            head, contents = contents[:first_index], contents[first_index + 4 :]
    else:
        head, contents = contents[first_index + 4 : second_index], contents[second_index + 4 :]

    try:
        attributes = tomli.loads(head)
    except TOMLDecodeError as e:
        print(f"could not load file {path}\n")
        raise e

    transformed, requested_images = transform(contents)

    return attributes, contents, transformed, requested_images
