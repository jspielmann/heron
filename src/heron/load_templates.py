"""
Load all available templates. Simple.
"""
from pathlib import Path

from jinja2 import Environment, select_autoescape, FileSystemLoader


def load_templates(path: Path) -> Environment:

    template_path = path / "_templates"

    env = Environment(loader=FileSystemLoader(template_path), autoescape=select_autoescape())

    return env
