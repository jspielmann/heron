"""
Handle raw files: just copy them into the target.
"""
import shutil
from pathlib import Path


def copy_raw_files(from_path: Path, to_path: Path):

    if from_path.exists():
        shutil.copytree(from_path, to_path, dirs_exist_ok=True)
