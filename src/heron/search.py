import json
import re
from pathlib import Path
from typing import List

from .build_collections import Collection


def generate_search_index(collection: Collection, output_path: Path):
    index = process_collection(collection, [])

    with open(output_path / "search_index.json", "w") as f:
        json.dump(index, f)

def process_collection(collection: Collection, index: List):

    for file in collection:
        raw_content = file["_raw_content"]
        raw_content = raw_content.replace("\n", " ")
        raw_content = re.sub(r"<style>.+?</style>", " ", raw_content)
        raw_content = re.sub(r"<.+?>", " ", raw_content)
        raw_content = re.sub(r"[^\w\s:/.@'-]", " ", raw_content)
        raw_content = re.sub(r"\s+", " ", raw_content).strip()

        index.append({
            "id": len(index),
            "title": get_title_for_file(file),
            "url": file["_url"],
            "content": raw_content,
        })

    for subcoll in collection.collections.values():
        process_collection(subcoll, index)

    return index

def get_title_for_file(file):

    title = ""

    if file.get("_parent"):
        parent: Collection = file["_parent"]
        if parent.attributes.get("title"):
            title = f"{parent.attributes['title']} / "

    title += file.get("title", "&langle;no title&rangle;")

    return title
